## pfarqed jQuery Credit Card Entry Plugin

For a discussion and example, see the [original post at pfarq.net](http://pfarq.net/blog/2011/11/17/jquery-credit-card-entry/)

This is a lightweight jQuery plugin for managing a credit card entry suitable for many types of payment gateways.

* Supports an address-less form.  Useful for systems where billing and shipping addresses are handled elsewhere.
* Reduces various prefix spaces for individual card types to a common issuer network.
* Uses an optimised Trie to index card prefixes and allow super fast network guessing.
* Supports a hidden 'network' field for forms that require the card type (i.e. all of them).
* Provides validation change events to drive enabling/disabling of submit buttons etc.
* Employs context on the root jQuery object to enable multiple instances (I doubt you'd need this, it's just good practice).

---


### Usage

To embed a card entry form, place the following inside your form.  The 'name' attributes can be replaced with the required field names for your form.

```html
<div id="pfcc-entry-form">
    <input type="hidden" class="pfcc-network" name="someform-type"></input>
    <div class="pfcc-card-icons"></div>
    <div>
        <input type="text" class="pfcc-number" name="someform-number" />
        <input type="text" class="pfcc-security" name="someform-security" />
    </div>
    <div>
        Expires
        <select class="pfcc-month" name="someform-month"></select>
        <select class="pfcc-year" name="someform-year"></select>
    </div>
    <ul class="pfcc-status"></ul>
</div>
```

Style the form using the supplied css, or your own.

```html
<link rel="stylesheet" href="css/pfarqedcc.css" type="text/css" />
```

jQueruncify the form with the incantation.

```html
<script type="text/javascript">
$(document).ready(function()
{
    $('#pfcc-entry-form').pfarqedcc(
    {
        networks: ['visa', 'mastercard']
    });

    $('#pfcc-entry-form').bind('validated', function()
    {
        // enable something here
    });

    $('#pfcc-entry-form').bind('invalidated', function()
    {
        // disable something here
    });
});
</script>
```

---


### Options

The default options are specified as :

```javascript
{
    imagePath: 'image',
    iconFadeDuration: 200,
    activeIconOpacity: 1.0,
    inactiveIconOpacity: 0.2,

    networks :
    [
        'visa',
        'mastercard',
        'amex',
        'discover'
    ],

    captions:
    {
        number: 'Credit Card Number',
        security: 'Security Code'
    },

    errors:
    {
        cardNetwork: 'We\'re sorry, but we currently don\'t accept {network}.',
        cardUnknownNetwork: 'The card number does not appear to be from a known network.',
        cardChars: 'The card number contains invalid characters.',
        cardLength: 'The card number is not the correct length.',
        cardInvalid: 'The card number is invalid.',
        securityChars: 'The security code contains invalid characters.',
        securityLength: 'The security code is not the correct length (should be {expected} digits).'
    },

    selectors :
    {
        network: '.pfcc-network',
        icons: '.pfcc-card-icons',
        number: '.pfcc-number',
        security: '.pfcc-security',
        month: '.pfcc-month',
        year: '.pfcc-year',
        status: '.pfcc-status'
    }
}
```

Everything except `networks` uses the recursive option of `$.extend()`.

You can adjust individual fields by just specifying the fields you want.  In this example the `networks` array is replaced, while `captions.number` and `selectors.status` will be overwritten without changing their siblings.

```javascrpt
$('#pfcc-entry-form').pfarqedcc(
{
	networks: ['visa', 'mastercard', 'amex'],
	captions :
	{
		number: 'Magic Buy Stuff Number'
	}
    selectors :
    {
    	status: '#my-specific-status-selector'
    }
});
```
