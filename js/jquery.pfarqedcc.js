/*!
 * jQuery pfarqed credit card plugin
 * https://github.com/pfarq/jquery-pfarqedcc-plugin/
 *
 * Copyright 2011, Llama
 * Licensed under the MIT license.
 * See MIT-LICENSE.txt for the complete text.
 */

(function ($) { $.fn.extend({

pfarqedcc: function (uopt)
{
    // IE doesn't implement indexOf.. bastards.

    if (!Array.prototype.indexOf)
    {
        Array.prototype.indexOf = function(elt)
        {
            var len = this.length >>> 0;

            var from = Number(arguments[1]) || 0;

            from = (from < 0)
                   ? Math.ceil(from)
                   : Math.floor(from);

            if (from < 0)
                from += len;

            for (; from < len; from++)
            {
                if (from in this && this[from] === elt)
                    return from;
            }

            return -1;
        };
    }

    var ckey = 'pfcc';

    // default options

    var defoptions =
    {
        imagePath: 'image',
        iconFadeDuration: 200,
        activeIconOpacity: 1.0,
        inactiveIconOpacity: 0.2,

        networks :
        [
            'visa',
            'mastercard',
            'amex',
            'discover'
        ],

        captions:
        {
            number: 'Credit Card Number',
            security: 'Security Code'
        },

        errors:
        {
            cardNetwork: 'We\'re sorry, but we currently don\'t accept {network}.',
            cardUnknownNetwork: 'The card number does not appear to be from a known network.',
            cardChars: 'The card number contains invalid characters.',
            cardLength: 'The card number is not the correct length.',
            cardInvalid: 'The card number is invalid.',
            securityChars: 'The security code contains invalid characters.',
            securityLength: 'The security code is not the correct length (should be {expected} digits).'
        },

        selectors :
        {
            network: '.pfcc-network',
            icons: '.pfcc-card-icons',
            number: '.pfcc-number',
            security: '.pfcc-security',
            month: '.pfcc-month',
            year: '.pfcc-year',
            status: '.pfcc-status'
        }
    };

    // instance state

    var defstate =
    {
        currentCardIndex: -1,
        currentCard: null,
        validated: false,

        number:
        {
            valid: false,
            error: null
        },

        security:
        {
            valid: false,
            error: null
        }
    };

    // public methods

    var methods =
    {
    };

    // card prefix index

    var index =
    {
        cardsLookup: null,

        cards:
        [
            { name: 'Visa', network: 'visa', lengths: [13,16], prefixes: [4]},
            { name: 'MasterCard', network: 'mastercard', lengths: [16], prefixes: [51,52,53,54,55]},
            { name: 'Diners Club', network: 'diners', lengths: [14,16], prefixes: [36,38]},
            { name: 'Diners Carte Blanche', network: 'diners', lengths: [14], prefixes: [300,301,302,303,304,305]},
            { name: 'American Express', network: 'amex', lengths: [15], prefixes: [34,37]},
            { name: 'Discover', network: 'discover', lengths: [16], prefixes: [6011,622,64,65]},
            { name: 'JCB', network: 'jcb', lengths: [16], prefixes: [3,1800,2131]},
            { name: 'Diners enRoute', network: 'diners', lengths: [15], prefixes: [2014,2149]},
            { name: 'Solo', network: 'solo', lengths: [16,18,19], prefixes: [6334, 6767]},
            { name: 'Switch', network: 'visa', lengths: [16,18,19], prefixes: [4903,4905,4911,4936,564182,633110,6333,6759]},
            { name: 'Maestro', network: 'maestro', lengths: [12,13,14,15,16,18,19], prefixes: [5018,5020,5038,6304,6759,6761]},
            { name: 'Visa Electron', network: 'visa', lengths: [16], prefixes: [417500,4917,4913,4508,4844]},
            { name: 'LaserCard', network: 'lasercard', lengths: [16,17,18,19], prefixes: [6304,6706,6771,6709]}
        ],

        indexCards: function()
        {
            index.cardsLookup = { leaves: {}, leafcount: 0 };

            $(index.cards).each( function(cindex, card)
            {
                $(card.prefixes).each( function(pindex, prefix)
                {
                    index.setValueForKey(prefix + '', cindex);
                });
            });
        },

        setValueForKey: function(key, value)
        {
            var leaf = index.cardsLookup;

            for(var n = 0; n < key.length; ++n)
            {
                var ch = key.charAt(n);

                if (!leaf.leaves.hasOwnProperty(ch))
                {
                    leaf.leaves[ch] = { parent:leaf, leaves:{}, leafcount:0 };
                    leaf.leafcount++;
                }

                leaf = leaf.leaves[ch];

                if (n + 1 == key.length)
                    leaf['value'] = value;
            }
        },

        valueForKey: function(key)
        {
            var leaf = index.cardsLookup;
            var lastMatch = -1;

            key += '';

            for (var n = 0; n < key.length; ++n)
            {
                var ch = key.charAt(n);

                if (!leaf.leaves.hasOwnProperty(ch))
                    return lastMatch;

                leaf = leaf.leaves[ch];

                if (leaf.hasOwnProperty('value'))
                    lastMatch = leaf.value;
            }

            if (leaf.leafcount > 0)
                return -1;

            return leaf.value;
        },

        luhn : function (number)
        {
            var ca, sum = 0, mul = 0;
            var len = number.length;

            while (len--)
            {
                ca = parseInt(number.charAt(len), 10) << mul;
                sum += ca - (ca > 9) * 9;
                mul ^= 1;
            };

            return (sum % 10 === 0) && (sum > 0);
        },

        guessCard: function()
        {
            var c = $(this).data('pfcc');
            var s = c.state;

            var cindex = index.valueForKey($(this).val().replace(/[^0-9]+/g, ''));

            if (cindex != c.state.currentCardIndex)
            {
                s.currentCardIndex = cindex;

                if (s.currentCardIndex < 0)
                {
                    s.currentCard = null;
                    c.elem.icons.children().fadeTo(c.opt.iconFadeDuration, 1.0);
                }
                else
                {
                    s.currentCard = index.cards[s.currentCardIndex];

                    var is = '.pfcc-icon-' + index.cards[s.currentCardIndex].network;

                    c.elem.icons.find(':not('+ is + ')').fadeTo(c.opt.iconFadeDuration, c.opt.inactiveIconOpacity);
                    c.elem.icons.find(is).fadeTo(c.opt.iconFadeDuration, c.opt.activeIconOpacity);
                }
            }
        },

        validateCard: function()
        {
            var c = $(this).data('pfcc');
            var s = c.state;

            $(this).each(index.guessCard);

            var end = function (valid, error)
            {
                s.number.valid = valid;
                s.number.error = error;

                ui.formatErrors(c);
            }

            var val = $(this).val();
            var num = val.replace(/[^0-9]+/g, '');

            if (val == $(this).data('pfcc-cap'))
                return end(false, null);

            if (val.match(/[^0-9 -]/))
                return end(false, c.opt.errors.cardChars);

            if (!s.currentCard)
                return end(false, c.opt.errors.cardUnknownNetwork);

            if (s.currentCard && c.opt.networks.indexOf(s.currentCard.network) < 0)
                return end(false, c.opt.errors.cardNetwork.replace(/\{network\}/g, s.currentCard.name));

            if (num.length > 0 && (!s.currentCard || s.currentCard.lengths.indexOf(num.length) < 0))
                return end(false, c.opt.errors.cardLength);

            if (!index.luhn(num))
                return end(false, c.opt.errors.cardInvalid);

            return end(true, null);
        },

        validateSecurity: function()
        {
            var c = $(this).data('pfcc');
            var s = c.state;

            var end = function (valid, error)
            {
                s.security.valid = valid;
                s.security.error = error;

                ui.formatErrors(c);
            }

            var val = $(this).val();

            if (val == $(this).data('pfcc-cap'))
                return end(false, null);

            if (val.length == 0)
                return end(false, null);

            if (val.match(/[^0-9]/))
                return end(false, c.opt.errors.securityChars);

            if (!s.currentCard)
                return end(false, null);

            var expected = (s.currentCard.network == 'amex') ? 4 : 3;

            if (val.length != expected)
            {
                return end(false, c.opt.errors.securityLength.replace(/\{expected\}/g, expected));
            }

            return end(true, null);
        }
    };

    var ui =
    {
        configure: function (c)
        {
            // cache ui elements

            $.each(c.opt.selectors, function (name, selector)
            {
                c.elem[name] = c.obj.find(selector);

                if (!c.elem[name].length)
                   $.error('Couldn\'t find element "' + selector + '"');
            });

            // set auto captions

            $.each(c.opt.captions, function(field, caption)
            {
                var elem = c.elem[field];

                elem.data('pfcc', c);

                elem.val(caption);
                elem.data('pfcc-cap', caption);

                elem.bind('focus', ui.clearCaptionOnFocus);
                elem.bind('blur', ui.restoreCaptionOnBlur);
            });

            // populate months combo box

            for (i = 1; i <= 12; ++i)
            {
                c.elem.month.append($('<option value="' + i + '">' + (i < 10  ? '0' : '') + i + '</option>'));
            }

            // populate years combo box

            for (i = 2011; i <= 2025; ++i)
            {
                c.elem.year.append($('<option value="' + i + '">' + i + '</option>'));
            }

            // populate icons

            $.each(c.opt.networks, function (i, n)
            {
                c.elem.icons.append($('<img class="pfcc-icon pfcc-icon-' + n + '" src="' + c.opt.imagePath + '/' + n + '.png">'));
            });

            // validation specific bindings

            c.elem.number.bind('keyup', index.guessCard);

            c.elem.number.bind('blur', index.validateCard);

            c.elem.security.bind('blur', index.validateSecurity);
        },

        clearCaptionOnFocus: function()
        {
            if ($(this).val() == $(this).data('pfcc-cap'))
            {
                $(this).val('');
            }
        },

        restoreCaptionOnBlur: function()
        {
            if ($(this).val() == '')
            {
                $(this).val($(this).data('pfcc-cap'));
            }
        },

        formatErrors: function(c)
        {
            c.elem.status.empty();

            $.each(['number', 'security'], function(i, key)
            {
                var s = c.state[key];

                if (s.error)
                {
                    c.elem.status.append('<li>' + s.error + '</li');
                    c.elem[key].addClass('pfcc-input-error');
                }
                else
                {
                    c.elem[key].removeClass('pfcc-input-error');
                }
            });

            var val = c.state.number.valid && c.state.security.valid;

            if (c.state.validated != val)
            {
                c.state.validated = val;
                c.obj.trigger(c.state.validated ? 'validated' : 'invalidated');
                c.elem.network.val(c.state.validated ? c.state.currentCard.network : '');
            }
        }
    };

    return this.each(function ()
    {
        if (typeof uopt === "string")
        {
            if (method[uopt])
            {
                return methods[uopt]($(this));
            }
            else
            {
                $.error('Method ' + uopt + ' does not exist on jQuery.pfarqedCCEntry');
            }
        }

        // configure context

        var ctx = $(this).data(ckey);

        if (!ctx)
        {
            ctx =
            {
                obj: $(this),
                opt: defoptions,
                state: defstate,
                elem: {},
                icons: {}
            };

            $(this).data(ckey, ctx);
        }

        // merge user defined options

        if (uopt)
        {
            ctx.opt = $.extend(true, ctx.opt, uopt);

            // replace networks

            if (uopt.networks)
              ctx.opt.networks = uopt.networks;
        }

        // index the card prefixes

        if (!index.cardsLookup)
        {
            index.indexCards();
        }

        // configure the plugin

        ui.configure(ctx);
    });
}});
}(jQuery));
